//
//  CurrentWeather.swift
//  WeatherAPP
//
//  Created by Tom Marler on 3/30/17.
//  Copyright © 2017 Tom Marler. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
    
    var _cityName: String!
    var _date: String!
    var _weatherType: String!
    var _currentTemp: Double!
    
    var cityName: String {
        if _cityName == nil {
            _cityName = ""
        }
        return _cityName
    }
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date = "Today, \(currentDate)"
    
        return _date
    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    
    var currentTemp: Double {
        if _currentTemp == nil {
            _currentTemp = 0.0
        }
        return _currentTemp
    }
    
    func downloadWeatherDetails(completed: @escaping DownloadComplete) {
        
        // Turn string into URL
        //let currentWeatherURL = URL(string: CURRENT_WEATHER_URL)

        // Have alamofire send a request to the URL inside the Constant
        // The request has a response
        // The response has a result
        
        Alamofire.request(CURRENT_WEATHER_URL).responseJSON { response in
            let result = response.result
            
            // Main JSON is dict
            if let dict = result.value as? Dictionary<String, AnyObject> {
                
                // Look through the dictionary for key called name
                if let name = dict["name"] as? String {
                    
                    // Set _cityName
                    self._cityName = name.capitalized
                    print(self._cityName)
                    
                }
                
                // Look through the dictionary for key called weather and cast it as array of dictionary
                if let weather = dict["weather"] as? [Dictionary<String, AnyObject>] {
                    
                    if let main = weather[0]["main"] as? String {
                        self._weatherType = main.capitalized
                        print(self._weatherType)
                    }
                }
                
                // Look through the dictionary for key called main and cast it as Dictionary
                if let main = dict["main"] as? Dictionary<String, AnyObject> {
                    
                    // Within the main dictionary look for a key called temp and cast it as a Double
                    if let currentTemperature = main["temp"] as? Double {
                        
                        let kelvinToFarenheithPreDivision = (currentTemperature * (9/5) - 459.67)
                        
                        let kelvinToFarenheith = Double(round(10 * kelvinToFarenheithPreDivision/10))
                        
                        self._currentTemp = kelvinToFarenheith
                        print(self._currentTemp)
                        
                    }
                }
            }
            print(response)
            completed()
        }
    }
}
