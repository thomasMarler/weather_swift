//
//  Constants.swift
//  WeatherAPP
//
//  Created by Tom Marler on 3/30/17.
//  Copyright © 2017 Tom Marler. All rights reserved.
//

import Foundation

// http://api.openweathermap.org/data/2.5/forecast/daily?lat=-33.87&lon=121.9&cnt=10&appid=cb52688185a70d169d126cb4eb73d51e
// http://api.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=cb52688185a70d169d126cb4eb73d51e

let BASE_URL = "http://api.openweathermap.org/data/2.5/weather?"
let LATITUDE = "lat="
let LONGITUDE = "&lon="
let APP_ID = "&appid="
let API_KEY = "cb52688185a70d169d126cb4eb73d51e"

typealias DownloadComplete = () -> ()

//let CURRENT_WEATHER_URL = "\(BASE_URL)\("lat=", Location.sharedInstance.latitude)\("&lon=", Location.sharedInstance.longitude)\(APP_ID)\(API_KEY)"
//
//let CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(Location.sharedInstance.latitude)&lon=\(Location.sharedInstance.longitude)&cnt=10&appid=cb52688185a70d169d126cb4eb73d51e"
////
//let FORECAST_URL = "http://api.openweathermap.org/data/2.5/weather?lat=\(Location.sharedInstance.latitude)&lon=\(Location.sharedInstance.longitude)&appid=cb52688185a70d169d126cb4eb73d51e"

let CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)&appid=cb52688185a70d169d126cb4eb73d51e"

let FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)&cnt=10&appid=cb52688185a70d169d126cb4eb73d51e"


//let CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?lat=\(Location.sharedInstance.latitude)&lon=\(Location.sharedInstance.longitude)&appid=42a1771a0b787bf12e734ada0cfc80cb"
//let FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(Location.sharedInstance.latitude)&lon=\(Location.sharedInstance.longitude)&cnt=10&mode=json&appid=42a1771a0b787bf12e734ada0cfc80cb"
