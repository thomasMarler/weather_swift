//
//  WeatherVC
//  WeatherAPP
//
//  Created by Tom Marler on 3/29/17.
//  Copyright © 2017 Tom Marler. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class WeatherVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentWeatherTypeLabel: UILabel!
    @IBOutlet weak var currentWeatherImage: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    // Generic Class
    var currentWeather: CurrentWeather!
    var forecast: Forecast!
    var forecasts = [Forecast]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Tell location manager how we want it to work
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        currentWeather = CurrentWeather()
        //forecast = Forecast()
        
        
        // TODO: Need to Work on this should be dynamic if you have problems it is something with CLLocation
        Location.sharedInstance.latitude = -33.87
        Location.sharedInstance.longitude = 121.9
        currentWeather.downloadWeatherDetails {
            
            self.downloadForecastData {
                self.updateMainUI()
            }

        }
        print(CURRENT_WEATHER_URL)
        print(FORECAST_URL)
        

    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        locationAuthStatus()
//    }
    
//    func locationAuthStatus() {
//        
//        // If authorized to get location
//        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
//            currentLocation = locationManager.location
//            //Location.sharedInstance.latitude = currentLocation.coordinate.latitude
//            //Location.sharedInstance.longitude = currentLocation.coordinate.longitude
//            
//            print("This is Location.sharedInstance.latitude\n")
//            print(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
//            
//            print("This is currentWeather\n")
//            print(currentWeather.cityName)
//            //print(currentWeather.cityName)
//            currentWeather.downloadWeatherDetails {
//                
//                self.downloadForecastData {
//                    self.updateMainUI()
//                }
//            }
//
//            
//        } else {
//            
//            // If not authorized request location
//            locationManager.requestWhenInUseAuthorization()
//            
//            locationAuthStatus()
//        }
//    }
    
    // Parse JSON from Response and build Forecast Object
    func downloadForecastData(completed: @escaping DownloadComplete){
        
        // Make at request to the forecast URL
        Alamofire.request(FORECAST_URL).responseJSON { response in
        
            // Store the response
            let result = response.result
            
            // Cast the result into a dictionary
            if let dict = result.value as? Dictionary<String, AnyObject> {
                
                // Within the dict search for a list string and cast it as array of Dictionary that can hold string AnyObject
                if let list = dict["list"] as? [Dictionary<String, AnyObject>] {
                    
                    // For every obj in the list
                    for obj in list {
                        
                        // Create a forecast object from the array
                        let forecast = Forecast(weatherDict: obj)
                        
                        // Append the forecast object to the arrray of forecasts objects
                        self.forecasts.append(forecast)
                        
                        // Print the object
                        print(obj)
                    }
                    
                    self.forecasts.remove(at: 0)
                    self.tableView.reloadData()
                }
            }
            
            // Done downloading
            completed()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecasts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Cast cell as weathercell
        if let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? WeatherCell {
        
            // take the cell index value and use to find the forecast for the cell
            let forecast = forecasts[indexPath.row]
            
            // configure the cell
            cell.configureCell(forecast: forecast)
            return cell
            
        } else {
            
            return WeatherCell()
        }
        
    }
    
    func updateMainUI() {
        dateLabel.text = currentWeather.date
        currentTempLabel.text = "\(currentWeather.currentTemp)"
        currentWeatherTypeLabel.text = currentWeather.weatherType
        locationLabel.text = currentWeather.cityName
        currentWeatherImage.image = UIImage(named: currentWeather.weatherType)
        
    }



}

