//
//  Forecast.swift
//  WeatherAPP
//
//  Created by Tom Marler on 3/30/17.
//  Copyright © 2017 Tom Marler. All rights reserved.
//

import UIKit
import Alamofire

class Forecast {
    
    var _date: String!
    var _weatherType: String!
    var _highTemp: String!
    var _lowTemp: String!
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        return _date
    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    
    var highTemp: String {
        if _highTemp == nil {
            _highTemp = ""
        }
        return _highTemp
    }
    
    var lowTemp: String {
        if _lowTemp == nil {
            _lowTemp = ""
        }
        return _lowTemp
    }
    
    
    // Gets a Dictionary with a String and AnyObject to create a forecast
    init(weatherDict: Dictionary<String, AnyObject>) {
        
        // Main dictionary
        if let temp = weatherDict["temp"] as? Dictionary<String, AnyObject> {
            
            // Within temp look for min string cast it as a double
            if let min = temp["min"] as? Double {
                
                // Covert Temp
                let kelvinToFarenheithPreDivision = (min * (9/5) - 459.67)
                
                let kelvinToFarenheith = Double(round(10 * kelvinToFarenheithPreDivision/10))
                
                // Store Low Temperature
                self._lowTemp = "\(kelvinToFarenheith)"

            }
            
            // Within temp look for string named max cast is as double
            if let max = temp["max"] as? Double {
                
                // Conver Temp
                let kelvinToFarenheithPreDivision = (max * (9/5) - 459.67)
                
                let kelvinToFarenheith = Double(round(10 * kelvinToFarenheithPreDivision/10))
                
                // Store high Temp
                self._highTemp = "\(kelvinToFarenheith)"
                
            }
        }
        
        // Within weatherDict look for string named weather cast it is as an Array of Dictionary that hold String and AnyObject
        if let weather = weatherDict["weather"] as? [Dictionary<String, AnyObject>] {
            
            // Within the first Array index look for string named main and cast it as a string
            if let main = weather[0]["main"] as? String {
                
                // Store weatherType
                self._weatherType = main
            }
        }
        
        // Convert Date
        if let date = weatherDict["dt"] as? Double {
            
            let unixConvertedDate = Date(timeIntervalSince1970: date)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "EEEE"
            dateFormatter.timeStyle = .none
            
            // Store date
            self._date = unixConvertedDate.dayOfTheWeek()
        }
    }
}


// Get the day of the week from the date
extension Date {
    
    func dayOfTheWeek() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}
