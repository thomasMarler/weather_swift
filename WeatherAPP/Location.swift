//
//  Location.swift
//  WeatherAPP
//
//  Created by Mike Witter on 3/31/17.
//  Copyright © 2017 Tom Marler. All rights reserved.
//

import CoreLocation

class Location {
    
    static var sharedInstance = Location()
    private init() { }
    
    var latitude: Double!
    var longitude: Double!
    
    
    
}
